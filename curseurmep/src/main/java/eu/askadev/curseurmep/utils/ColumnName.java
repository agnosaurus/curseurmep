package eu.askadev.curseurmep.utils;

import java.util.Arrays;
import java.util.Optional;

public enum ColumnName {

	Baseline_1("Baseline 1","baseline",1),
	Baseline_2("Baseline 2","baseline2",2),
	Post_TMS("Post-TMS","postTMS",3),
	FU1("FU1","FU1",4),
	FU2("FU2","FU2",5);


	ColumnName(String excelName, String fileName,int columnNb) {
		this.excelName = excelName;
		this.fileName = fileName;
		this.columnNb = columnNb;
	}

	public String excelName;
	public String fileName;
	public int columnNb;


	public String getExcelName() {
		return excelName;
	}


	public int getColumnNb() {
		return columnNb;
	}



	public void setColumnNb(int columnNb) {
		this.columnNb = columnNb;
	}



	public void setExcelName(String excelName) {
		this.excelName = excelName;
	}



	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public static Optional<ColumnName> getFrom(String columnFile) {
		return Arrays.stream(ColumnName.values()).filter( colName -> colName.getFileName().equals(columnFile) ).findFirst();
	}
}
