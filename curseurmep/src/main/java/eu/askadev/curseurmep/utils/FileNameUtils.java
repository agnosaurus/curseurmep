package eu.askadev.curseurmep.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;


public class FileNameUtils {


	private static final int BLOCK_SIZE = 16;



	public static String getDirNameFromPath(String filePath) {
		String[] paths = filePath.split("\\\\");
		return paths[paths.length-5];
	}


	//sub-P058-1967_t01_baseline
	// P058-//  P058_1967
	public static List<String> getSheetNamesFromDirName(String dirName) {

		List<String> result = new ArrayList<String>();

		String part0 = dirName.replaceAll("sub-", "").split("_")[0];
		String[] subPart = part0.split("-");


		result.add(subPart[0]+"_");

		if (subPart.length>1)
			result.add(subPart[0]+"_"+subPart[1]);


		return result;
	}



	//sub-P058-1967_t01_baseline => Baseline
	public static ColumnName getColumnNameFromDirName(String dirName) {
		String columFromFile = dirName.split("_")[dirName.split("_").length-1];

		if ( ColumnName.getFrom(columFromFile).isPresent())
			return ColumnName.getFrom(columFromFile).get();

		return null; //TODO : cas quand ca matche pas
	}




	public static String getFilenameFromFile(File file) {
		return FilenameUtils.removeExtension(file.getName());
	}

	public static List<String> getFilenamesFromFiles(List<File> files) {
		return files.stream()
				.map(file -> getFilenameFromFile(file))
				.collect(Collectors.toList());

	}


	// ICF_7ms--Muscle__054 => ICF_7
	public static String getAcquisitionType(String fileName) {
		String[] parts = fileName.split("--");
		return parts[0].replace("ms_", "").replace("ms", "");
	}
	
	// ICF_7ms--Muscle__054 => ICF_7
	public static BlocName getBlocName(String fileName) {
		return BlocName.getBlocName(getAcquisitionType(fileName));
	}


	// ICF_7ms--Muscle__054 => 54
	public static Integer getIndexFromFilename(String fileName) {	

		String cleanedFileName =  FilenameUtils.removeExtension(fileName.replaceAll(".csv", ""));
		return Integer.parseInt(cleanedFileName.split("_")[cleanedFileName.split("_").length-1]);
	}


	public static int computeLocalIndex(File file , List<File> files) {

		String fileName =getFilenameFromFile(file);

		int fileIndex = getIndexFromFilename(fileName);
		BlocName fileColumnName = FileNameUtils.getBlocName(fileName);

		Map<BlocName, Integer> startIndexes = computeStartIndexes(files);
		return fileIndex-startIndexes.get(fileColumnName);

	}

	public static boolean isFilenameValid(String filename) {
		return filename.contains("--") && filename.contains("__");
	}



	//TODO may need cache ?
	public static Map<BlocName, Integer> computeStartIndexes(List<File> files){

		Map<BlocName, Integer> startIndexes = new HashMap<>();

		for (int i =0 ; i< files.size() ; i++) {
			File file = files.get(i);
			String fileName =getFilenameFromFile(file);

			int index = FileNameUtils.getIndexFromFilename(fileName);
			BlocName blocName = FileNameUtils.getBlocName(fileName);

			if ( !startIndexes.containsKey(blocName) || startIndexes.get(blocName) > index){
				startIndexes.put(blocName, index);
			}

		}

		return startIndexes;
	}

}
