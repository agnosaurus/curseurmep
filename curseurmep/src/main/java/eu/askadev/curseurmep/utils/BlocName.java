package eu.askadev.curseurmep.utils;

import java.util.Arrays;

public enum BlocName {

	
  TF("Baseline",10),
  ICF_7("ICF_7",79),
  ICF_15("ICF_15",102),
  SICI_1("SICI_1",31),
  SICI_3("SICI_3",55)
  ;
	
	String key;
	int startRow;
	
	
	private BlocName(String key, int startRow) {
		this.key = key;
		this.startRow = startRow;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public int getStartRow() {
		return startRow;
	}


	public static BlocName getBlocName(String blocName) {
		return Arrays.stream(BlocName.values())
				.filter(bn -> bn.getKey().equals(blocName))
				.findFirst()
				.orElse(null);
	}
	
}
