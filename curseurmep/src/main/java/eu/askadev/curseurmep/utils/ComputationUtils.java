package eu.askadev.curseurmep.utils;

public class ComputationUtils {

    /**
     * Compute the time equivalent of a point in the data
     *
     * @param totalTime total time of the series, in ms ( 100 or 200 )
     * @param index of the point in the serie
     * @return the time equivalent
     */
    public static double computeTime(int totalTime, int index){
        return (double) index * totalTime / 4800d  ;
    }
}
