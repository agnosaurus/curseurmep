package eu.askadev.curseurmep.utils;

import eu.askadev.curseurmep.processing.TimeDirection;

import java.util.ArrayList;

import static eu.askadev.curseurmep.processing.TimeDirection.*;

public class ProcessorUtils {



    public static final int NOISE_TRIGGER = 50;
    public static final int NOISE_INCREMENT = 40;

    // Once we found a spike, we set the start NOISE_INCREMENT later, then check if we got another spike
    public static int findTimeAfterSpike(ArrayList<Double> derivate){
        int afterPeekTime = 0;
        for (int j =0 ; j< derivate.size() ; j++){
            if (Math.abs(derivate.get(j)) > NOISE_TRIGGER){


                afterPeekTime = j + NOISE_INCREMENT;
                System.out.println("SpikeFOUND "+afterPeekTime );
            }
        }

        return afterPeekTime;
    }


    public static double computeWindowMean(ArrayList<Double> input, int index, int windowSize, TimeDirection direction){
        double result = 0;

        if (PAST == direction){
            for (int i = 0 ; i < windowSize ; i++){
                result+=input.get(index-i)/windowSize;
            }

        } else if(FUTURE == direction) {
            for (int i = 0 ; i < windowSize ; i++){
                result+=input.get(index+i)/windowSize;
            }
        } else if(PRESENT == direction) {
            result+= ( computeWindowMean(input,index,windowSize,PAST) + computeWindowMean(input,index,windowSize,FUTURE) ) /2;
        }



      //  System.out.println("window mean "+index+"|"+result );

        return result;

    }


}
