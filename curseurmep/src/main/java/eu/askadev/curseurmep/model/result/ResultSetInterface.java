package eu.askadev.curseurmep.model.result;


/**
 * @author pvincen
 *  result of an analysis.. can be a curve, a value
 */
public interface ResultSetInterface {
	
	public boolean printToFile() ;
	
	public String toString();
	
	
}
