package eu.askadev.curseurmep.model;

import java.util.ArrayList;
import java.util.HashMap;

import eu.askadev.curseurmep.gui.graph.CursorCollection;



/**
 * @author pvincen
 * know all the acquisition
 */
public class AcquisitionCollection extends HashMap<Acquisition,CursorCollection> {


	private static AcquisitionCollection instance;
	
	
	public static AcquisitionCollection getInstance(){
		
		if (instance == null)
			instance = new AcquisitionCollection();
		
		return instance;
		
	}
	
	
	private AcquisitionCollection(){
		super();
		
	}
	
	public void addAcquisition(Acquisition acq){
		
	}
	
	
	public void deleteAcquisition(Acquisition acq){
		
	}
	
	

}
