package eu.askadev.curseurmep.model;


import java.io.File;

/**
 * @author pvincen
 * Signal to analyse
 */


import java.nio.file.Files;
import java.util.*;

import eu.askadev.curseurmep.export.ExportDestination;
import eu.askadev.curseurmep.gui.graph.CursorCollection;
import eu.askadev.curseurmep.utils.FileNameUtils;


public class Acquisition {


	private ArrayList<Double> EMG;
	private String filename="";
	private ExportDestination exportDestination;


	public String getFilename() {
		return filename;
	}

	public ArrayList<Double> getEMG() {
		return EMG;
	}

	public Acquisition(File file) throws Exception {

		if (file!=null && FileNameUtils.isFilenameValid(file.getName())){
			this.filename= file.getName();
			EMG = new ArrayList<Double>(4800);

			Files.lines(file.toPath()).forEach(e->EMG.add(Double.parseDouble(e)));
			
			setExportDestination(new ExportDestination(file));
		}
	}
	

	public ExportDestination getExportDestination() {
		return exportDestination;
	}



	public void setExportDestination(ExportDestination exportDestination) {
		this.exportDestination = exportDestination;
	}

	@Override
	public String toString() {
		return "Acquisition{"+filename+"}";
	}

	public String getKey(){
		return filename;
	}
}
