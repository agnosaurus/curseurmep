package eu.askadev.curseurmep.gui.graph;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;

import eu.askadev.curseurmep.gui.DataPanel;




/**
 * @author pvincen
 * As a cursor, he knows:
 * 	- his abscisse(time), which is the rank in the dataset
 *   - which curve he belongs to
 *   - his color
 *   - his Marker object
 */
public class CursorGraph {

	
	Font labelFront = new Font("Arial",Font.BOLD,14);

	/************************************************
	 ************   Elements of the cursor ********** 
	 ************************************************/

	private Color color;
	private XYSeries curve;
	private String chartTitle;
	private double XValue; // value en Pixel
	private Marker marker;
	private DataPanel graphPanel;
	private String label;

public CursorGraph(){
	super();
}
	

	
	
	public void fillWithPixelValue(String label,Color colorInit, double XValue,String chartInit,DataPanel panel,JFreeChart chart) {
		
		fillCommon(colorInit,chartInit,panel);
			
		this.XValue = XValue;
		marker = new ValueMarker(GraphUtils.pixel2graph(XValue,chart));
		this.label=label;

		drawCursor();
	}
	
	
	public void fillCursorWithDataValue(String label,Color colorInit, double dataValue,String chartInit,DataPanel panel,JFreeChart chart) {
		
		fillCommon(colorInit,chartInit,panel);

		XValue = GraphUtils.data2pixel(dataValue,chart);
		marker = new ValueMarker(GraphUtils.data2graph(dataValue, chart));
		this.label=label;

		drawCursor();
	}
	
	
	private void fillCommon(Color colorInit,String chartInit,DataPanel panel){
		color = colorInit;
		chartTitle = chartInit;
		graphPanel = panel;
	}
	

	public void drawCursor(){
		
		//Draw a vertical line where the user has clicked, on the right chart

		ChartPanel chartPanel = graphPanel.getChartPanel(chartTitle);
		JFreeChart chart= chartPanel.getChart();

		XYPlot plot = chart.getXYPlot();

	
		marker.setPaint(color);
		marker.setLabel(label);
		marker.setLabelFont(labelFront);
		plot.addDomainMarker(marker);

	}

	public void eraseCursor(){
		
		ChartPanel chartPanel = graphPanel.getChartPanel(this.chartTitle);
		JFreeChart chart= chartPanel.getChart();
		XYPlot plot = chart.getXYPlot();
		plot.removeDomainMarker(this.marker);
		
		

	}


	//Accessors
	public String getChartTitle(){
		return chartTitle;
	}


	public Color getColor(){
		return color;
	}


	public double getXValue(){
		return XValue ;
	}

	public Marker getMarker(){
		return marker;

	}



	public String getLabel(){
		return label;

	}



}