package eu.askadev.curseurmep.gui.graph;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import eu.askadev.curseurmep.gui.MainFrame;
import eu.askadev.curseurmep.model.Acquisition;


/**
 * 
 * @author pvincent
 * 3 Coord
 *  - Time : valeur sur l axe X, en ms
 *  - Graph : 
 *  - Pixel : coordX du pixel ( entre PLOT_LEFT_MARGIN et PLOT_LEFT_MARGIN + PLOT_X_SIZE )
 */

public class GraphUtils {

	public final static double DATASET_LENGTH = 4800d;
	public static double GRAPH_TOTAL_LENGTH =0;
	public static double GRAPH_START_VALUE = 0;
	public static double GRAPH_END_VALUE = 0;
	public static double DATA_MAX_TIME = 100d; // We assume total is 100, then applty a factor depending on the real scale at the end of the calculations
	public final static double PLOT_LEFT_MARGIN = 80;
	public final static double PLOT_RIGHT_MARGIN = 15;
	public final static double PLOT_X_SIZE = 1200-(PLOT_LEFT_MARGIN+PLOT_RIGHT_MARGIN); // CHIFFRE MAGIQUE

	
	/**
	 * 
	 * @param xGraph : coordonnes pixel sur le graph 
	 * @param chart
	 * @return XTime
	 */
	public static double pixel2graph(double xGraph,JFreeChart chart){

		computeGraphLength(chart);

		double coord = GRAPH_START_VALUE+ (xGraph-80)*(GRAPH_END_VALUE-GRAPH_START_VALUE)/PLOT_X_SIZE;

		System.out.println("PIXEL2GRAPH IN "+xGraph);
		System.out.println("PIXEL2GRAPH OUT "+coord);

		return coord;
	}

	/**
	 * 
	 * @param data : data point index
	 * @param chart
	 * @return XTime
	 */
	public static double data2graph(double data,JFreeChart chart){

		double graph = (Double) data *DATA_MAX_TIME /DATASET_LENGTH;
		System.out.println("DATA2GRAPH IN "+data);
		System.out.println("DATA2GRAPH OUT "+graph);

		return graph;
	}


	public static int graph2data(double graph){

		
		int data = (int) (graph * DATASET_LENGTH / DATA_MAX_TIME) ;

		return data;
	}
	
	

	public static double data2pixel(double data,JFreeChart chart){

		double pixel =0;

		computeGraphLength(chart);


		pixel = PLOT_LEFT_MARGIN+data*(PLOT_X_SIZE-PLOT_LEFT_MARGIN-PLOT_RIGHT_MARGIN)/DATASET_LENGTH;

		
		System.out.println("DATA2PIXEL IN "+data);
		System.out.println("DATA2PIXEL OUT "+pixel);
		
		return pixel;

	}




	public static double pixel2time(double xPlotCoord,JFreeChart chart){

		System.out.println("plotXCoord2Graph IN "+xPlotCoord);
	

		XYPlot plot = (XYPlot) chart.getPlot();
		Range xRange = plot.getDomainAxis().getRange();


		double graphCoord =0;

		computeGraphLength(chart);


		if (xPlotCoord>PLOT_LEFT_MARGIN && xPlotCoord<(PLOT_X_SIZE-PLOT_RIGHT_MARGIN))
			graphCoord = GRAPH_START_VALUE+(xPlotCoord-PLOT_LEFT_MARGIN)*GRAPH_TOTAL_LENGTH/(PLOT_X_SIZE-PLOT_LEFT_MARGIN-PLOT_RIGHT_MARGIN);
		else
			System.out.println("Clic en dehors du graphique");

		//  System.out.println("Translating coord "+xPlotCoord+"into "+graphCoord);



		System.out.println("plotXCoord2Graph OUT "+graphCoord);
		
		return graphCoord;
	}


	private static void  computeGraphLength(JFreeChart chart){
		GRAPH_TOTAL_LENGTH= chart.getXYPlot().getDomainAxis().getRange().getLength();
		GRAPH_START_VALUE = chart.getXYPlot().getDomainAxis().getRange().getLowerBound();
		GRAPH_END_VALUE = chart.getXYPlot().getDomainAxis().getRange().getUpperBound();

		System.out.println(GRAPH_START_VALUE+" - "+GRAPH_END_VALUE +" - "+GRAPH_TOTAL_LENGTH);
	}


}
