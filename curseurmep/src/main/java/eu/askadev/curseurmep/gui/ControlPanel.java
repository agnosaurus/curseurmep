package eu.askadev.curseurmep.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileSystemView;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import eu.askadev.curseurmep.export.ExcelExporter;
import eu.askadev.curseurmep.utils.FileNameUtils;

public class ControlPanel extends JPanel implements ActionListener {

	/******************************************************
	 *****************   Graphic elements ***************** 
	 ******************************************************/

	private JPanel buttonPanel;

	private JComboBox comboTotalTime;

	private JButton loadFile;
	private JButton loadExportFile;
	private JButton exportResults;
	private JButton computeCursors;
	private JButton computeResults;
	private JButton closeTabs;
	private JLabel exportFileLabel = new JLabel();

	private List<File> openedFiles= new ArrayList<>();
	private File excelExportFile;
	private JLabel totalTimeLabel = new JLabel("Total time");




	/*************************************************************************
	 ************   Element about the current acquisition displayed ********** 
	 ************************************************************************/


	public ControlPanel() {

		super();

		this.setLayout(new FlowLayout());

		//Create panel for button
		buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		loadFile = new JButton("Load new files");
		loadFile.setActionCommand("LoadFile");

		Integer[] options = { 100,200  };
		comboTotalTime = new JComboBox(options);
		comboTotalTime.setToolTipText("Duree totale de l acquisition");
		comboTotalTime.setSelectedIndex(0);



		loadExportFile = new JButton("Load export file");
		loadExportFile.setActionCommand("loadExportFile");

		computeCursors = new JButton("Compute cursors");
		computeCursors.setActionCommand("ComputeCursors");

		computeResults = new JButton("Compute results");
		computeResults.setActionCommand("ComputeResults");



		exportResults = new JButton("Export results");
		exportResults.setActionCommand("exportResults");

		closeTabs = new JButton("Close all tabs");
		closeTabs.setActionCommand("CloseTabs");

		buttonPanel.add(loadFile);
		buttonPanel.add(computeCursors);
		buttonPanel.add(totalTimeLabel);
		buttonPanel.add(comboTotalTime);
		buttonPanel.add(computeResults);
		buttonPanel.add(loadExportFile);
		buttonPanel.add(exportResults);

		buttonPanel.add(closeTabs);
		buttonPanel.add(exportFileLabel);

		loadExportFile.addActionListener(this);
		loadFile.addActionListener(this);
		computeCursors.addActionListener(this);
		computeResults.addActionListener(this);


		closeTabs.addActionListener(this);
		exportResults.addActionListener(this);


		comboTotalTime.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					computeCursors.doClick();
					computeResults.doClick();
				}
			}
		});

		this.add(buttonPanel);

	}


	public int getTotalTime(){
		return Integer.parseInt(comboTotalTime.getSelectedItem().toString());
	}

	public void actionPerformed(ActionEvent e) {

		if ("ComputeCursors".equals(e.getActionCommand())) {
			getMainframe().getDataPanels().forEach(DataPanel::triggerCursor);
			JOptionPane.showMessageDialog(getMainframe(), "Cursors done","Info",JOptionPane.INFORMATION_MESSAGE);

		} else if("ComputeResults".equals(e.getActionCommand())) {
			getMainframe().getDataPanels().forEach(dataPanel -> dataPanel.getResultPanel().computeResult());
			JOptionPane.showMessageDialog(getMainframe(), "Results done","Info",JOptionPane.INFORMATION_MESSAGE);

		} else if("CloseTabs".equals(e.getActionCommand())) {
			MainFrame.getInstance().reset();
			openedFiles.clear();
			getMainframe().closeTabs();
		} else if("LoadFile".equals(e.getActionCommand())) {

			File latestFileDir=getMainframe().getLatestFileDir();

			if ( latestFileDir==null)
				latestFileDir=FileSystemView.getFileSystemView().getHomeDirectory();


			JFileChooser jfc = new JFileChooser(latestFileDir.getAbsolutePath());
			jfc.setMultiSelectionEnabled(true);
			jfc.setDialogTitle("Select data files to analyse");


			int returnValue = jfc.showOpenDialog(null);

			if (returnValue == JFileChooser.APPROVE_OPTION) {

				File[] files = jfc.getSelectedFiles();


				Arrays.stream(files)
						.filter(file -> FileNameUtils.isFilenameValid(file.getName()))
						.forEach( selectedFile -> {
							System.out.println(selectedFile.getAbsolutePath());
							addOpenedFiles(selectedFile);
						});


				Arrays.stream(files)
						.filter(file -> FileNameUtils.isFilenameValid(file.getName()))
						.forEachOrdered( selectedFile -> {
							getMainframe().setLatestFileDir(selectedFile.getParentFile());
							getMainframe().initAcq(selectedFile);
						});

			}

		} else if("loadExportFile".equals(e.getActionCommand())) {
			loadExportfile();
		} else if("exportResults".equals(e.getActionCommand())) {


			/**
			 * Export CSVs
			 */
			getMainframe().getDataPanels().forEach(dataPanel ->dataPanel.getResultPanel().exportAutomaticResult());


			if (excelExportFile==null)
				loadExportfile();

			// On cree une sauvegarde
			//Make a SAVE of file before
			File copied = new File(excelExportFile.getPath()+".SAVE");

			ExcelExporter.copyFile(excelExportFile,copied);
			HSSFWorkbook wb = ExcelExporter.getInstance().readFile(excelExportFile.getPath());

			// on boucle sur chaque acquisition
			getMainframe().getDataPanels()
					.forEach(dataPanel ->
							ExcelExporter.getInstance().writeData(wb,
									excelExportFile,
									dataPanel.getCurrentAcquisition().getExportDestination(),
									dataPanel.getResultPanel().getExportData())
					);

			try {
				ExcelExporter.getInstance().saveWorkbook(wb,excelExportFile);
			} catch (FileNotFoundException exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
			} catch (IOException exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
			}

			JOptionPane.showMessageDialog(getMainframe(), "Export done","Export",JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void loadExportfile() {

		File latestFileDir=getMainframe().getLatestFileDir();

		if ( latestFileDir==null)
			latestFileDir=FileSystemView.getFileSystemView().getHomeDirectory();


		JFileChooser jfc = new JFileChooser(latestFileDir.getAbsolutePath());
		jfc.setMultiSelectionEnabled(false);
		jfc.setDialogTitle("Select XLS export file");

		int returnValue = jfc.showOpenDialog(null);

		if (returnValue == JFileChooser.APPROVE_OPTION) {

			File file = jfc.getSelectedFile();
			excelExportFile=file;

			exportFileLabel.setText("Export to "+file.getName());

		}
	}


	private MainFrame getMainframe(){
		return MainFrame.getInstance();
	}

	public List<File> getOpenedFiles() {
		return openedFiles;
	}

	public void addOpenedFiles(File file) {
		openedFiles.add(file);
	}

}
