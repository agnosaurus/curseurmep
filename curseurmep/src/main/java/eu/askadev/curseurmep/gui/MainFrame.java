package eu.askadev.curseurmep.gui;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import eu.askadev.curseurmep.gui.graph.CursorCollection;
import eu.askadev.curseurmep.model.Acquisition;


public class MainFrame extends JFrame{


	private JTabbedPane tabbedPane = new JTabbedPane();
	ControlPanel controlPanel = new ControlPanel();

	private Map<String, CursorCollection> acquisitionCollection = new HashMap<String,CursorCollection>() ;
	private List<DataPanel> dataPanels = new ArrayList<>();



	private File latestFileDir=null;
	private static MainFrame instance = null;


	public static MainFrame getInstance() {
		if (instance==null)
			instance = new MainFrame();

		return instance;
	}


	public void reset() {
		acquisitionCollection.clear();
		dataPanels.clear();
	}


	public File getLatestFileDir() {
		return latestFileDir;
	}

	public ControlPanel getControlPanel() {
		return controlPanel;
	}

	public void setLatestFileDir(File latestFileDir) {
		this.latestFileDir = latestFileDir;
	}

	private MainFrame() {
		super("Analyse MEP");

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		firstTab();

		this.getContentPane().add(tabbedPane);


		//Display the window
		this.pack();
		this.setVisible(true);
	}

	public void closeTabs() {
		tabbedPane.removeAll();
		dataPanels.clear();
		firstTab();
	}


	private void firstTab(){
		tabbedPane.add("Control tab",controlPanel);
	}


	public void initAcq(File file){

		try {

			Acquisition acq = new Acquisition(file);

			DataPanel dataPanel = createDataPanel(acq);

			tabbedPane.add(title(acq),dataPanel);
			dataPanels.add(dataPanel);
			CursorCollection col1 = new CursorCollection();

			acquisitionCollection.put(acq.getKey(), col1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.pack();
		this.setVisible(true);
	}

	private DataPanel createDataPanel(Acquisition currentAcq){
		return new DataPanel(currentAcq);
	}

	public CursorCollection getCursorCollection(Acquisition acq){
		return acquisitionCollection.get(acq.getKey());
	}

	public List<DataPanel> getDataPanels() {
		return dataPanels;
	}


	public void setDataPanelColor(String acqFileName,Color color){
		for ( int i = 1 ; i<tabbedPane.getTabCount(); i++){
			if(acqFileName.equals(tabbedPane.getTitleAt(i))){
				tabbedPane.setForegroundAt(i, color);
			}
		}
	}

	public void removePanel(DataPanel dataPanel){
		acquisitionCollection.remove(dataPanel.getCurrentAcquisition().getKey());
		dataPanels.remove(dataPanel);

		for ( int i = 1 ; i<tabbedPane.getTabCount(); i++){
			if(title(dataPanel.getCurrentAcquisition()).equals(tabbedPane.getTitleAt(i))){
				tabbedPane.remove(i);
			}
		}

	}


	private String title(Acquisition acq){
		return acq.getFilename();
	}


}
