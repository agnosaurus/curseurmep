package eu.askadev.curseurmep.gui.graph;

import java.awt.Color;

/**
 * @author pvincen
 * represents a type of curve
 * EG : grip force Left
 * It has a name, a color, a cursor number
 * It may have other parameters ( EG : chart config).
 * 
 */
public class GraphCurve {

	private String name;
	private Color color;
	private int cursorNbr;

	public GraphCurve(String name,Color color, int cursorNbr) {
		this.name = name;
		this.color = color;
		this.cursorNbr= cursorNbr;
	}


	//Getters
	public Color getColor(){
		return this.color;
	}

	public String getname(){
		return this.name;
	}

	public int getCursorNbr(){
		return cursorNbr;
	}

	//Setters
	public void setCursorNbr(int nbr){
		cursorNbr=nbr;
	}

	public void setColor(Color clr){
		color=clr;
	}

	public void setName(String name){
		this.name = name;
	}
}
