package eu.askadev.curseurmep.gui.graph;


import java.util.ArrayList;
import java.util.Iterator;

import org.jfree.chart.JFreeChart;


/**
 * @author pvincen
 * knows all the cursor
 */
public class CursorCollection extends ArrayList<CursorGraph> {



	public double baseline =0d;
	
	/**
	 * @param collec collection to draw
	 */
	public static void drawCursorCollection(CursorCollection collec){

		// loop on each Cursoer, then draw it !
				
		Iterator<CursorGraph> it = collec.iterator();
		
		while (it.hasNext()){
			CursorGraph currentCursor = it.next();
			currentCursor.drawCursor();
			
		}
		
		
				
	}
	


	public CursorGraph findCursor ( double graphCoord,String chartTitle,JFreeChart chart){

		Iterator<CursorGraph> it = this.iterator();
		CursorGraph closestCursor=null;

		double min=1500;
		double dist;

		while (it.hasNext()){
			CursorGraph currentCursor = it.next();

			
			dist = Math.abs(GraphUtils.pixel2graph(currentCursor.getXValue(),chart) - graphCoord);

			//System.out.println("   ");
		//	System.out.println("Dist = "+dist);
			//System.out.println("Min = "+min);


			if ( dist < min && chartTitle.equals(currentCursor.getChartTitle()) ){
				closestCursor= currentCursor;
				min = dist;
			}
		}


		return closestCursor;
	}

	public void addCursor(CursorGraph cursor){

		this.add(cursor);
		//System.out.println("Collection size :"+cursorList.size());
	}


	public void removeCursor(CursorGraph cursor) {
		this.remove(cursor);
		//	System.out.println("Collection size :"+cursorList.size());
	}
	
	public void clearCursor(){
		
		Iterator<CursorGraph> it = this.iterator();
		
		while (it.hasNext()){
			CursorGraph currentCursor = it.next();
			currentCursor.eraseCursor();
			
		}
		
				
		this.clear();
	}
	
	
	
	public double getBaseline(){
		return baseline;
	}
	
	
	
	
	public void setBaseline(double bs){
		this.baseline = bs;
	}
	
	

}



