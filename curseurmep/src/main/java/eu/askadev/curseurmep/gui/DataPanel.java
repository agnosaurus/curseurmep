package eu.askadev.curseurmep.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.text.NumberFormatter;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import eu.askadev.curseurmep.gui.graph.CursorGraph;
import eu.askadev.curseurmep.gui.graph.DataChartManager;
import eu.askadev.curseurmep.gui.graph.GraphUtils;
import eu.askadev.curseurmep.model.Acquisition;
import eu.askadev.curseurmep.processing.stats.CursorProcessor;



public class DataPanel extends JPanel implements ActionListener,ChartMouseListener { 

	/******************************************************
	 *****************   Graphic elements ***************** 
	 ******************************************************/

	private JPanel buttonPanel;
	private JPanel graphPanel;


	private JButton b_addDebut;
	private JButton b_addFin;
	private JButton b_addMax;
	private JButton b_addMin;

	private JButton deleteCursor;
	private JButton clearCursor;
	private JButton computeCursor;
	private JButton cursorToZero;

	private JButton showInfo;
	private JButton closePanel;

	private String curCursor="";

	private static final int DEFAULT_MODE = 0;
	private static final int ADD_CURSOR = 1;
	private static final int DELETE_CURSOR = 20;

	/*************************************************************************
	 ************   Element about the current acquisition displayed ********** 
	 ************************************************************************/

	private int cursorMode= DEFAULT_MODE;

	// Data elements
	private Acquisition currentAcq;
	private ResultPanel resultPanel;

	DataChartManager dataChartManager= new DataChartManager();


	public DataPanel(Acquisition acq) {

		super();


		currentAcq = acq;
		this.setLayout(new FlowLayout());

		//Create panel for button
		buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));


		//Create a panel for the graph
		graphPanel = new JPanel();
		graphPanel.setLayout(new BoxLayout(graphPanel, BoxLayout.Y_AXIS));
		graphPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


		dataChartManager.initDataset(acq);
		ChartPanel chart1 = dataChartManager.getChart(1,acq.getFilename());

		graphPanel.add(chart1);



		//buttons definition
		b_addDebut = new JButton("Add cursor Start");
		b_addDebut.setActionCommand("addDebut");

		b_addFin = new JButton("Add cursor End");
		b_addFin.setActionCommand("addFin");


		b_addMax = new JButton("Add cursor Max");
		b_addMax.setActionCommand("addMax");


		b_addMin = new JButton("Add cursor Min");
		b_addMin.setActionCommand("addMin");

		deleteCursor = new JButton("Delete cursor");
		deleteCursor.setActionCommand("DeleteCursor");

		clearCursor = new JButton("Clear cursor");
		clearCursor.setActionCommand("ClearCursor");

		computeCursor = new JButton("Compute cursor");
		computeCursor.setActionCommand("ComputeCursor");

		cursorToZero = new JButton("Every cursor to zero");
		cursorToZero.setActionCommand("CursorToZero");


		showInfo = new JButton("Debug info");
		showInfo.setActionCommand("DebugInfo");

		closePanel = new JButton("Close panel");
		closePanel.setActionCommand("ClosePanel");


		buttonPanel.add(b_addDebut);
		buttonPanel.add(b_addFin);
		buttonPanel.add(b_addMax);
		buttonPanel.add(b_addMin);

		buttonPanel.add(deleteCursor);
		buttonPanel.add(clearCursor);
		buttonPanel.add(computeCursor);
		buttonPanel.add(cursorToZero);
		buttonPanel.add(showInfo);
		buttonPanel.add(closePanel);
		

		chart1.addChartMouseListener(this);
		//	chart2.addChartMouseListener(this);

		b_addDebut.addActionListener(this);
		b_addFin.addActionListener(this);
		b_addMax.addActionListener(this);
		b_addMin.addActionListener(this);
		cursorToZero.addActionListener(this);
		computeCursor.addActionListener(this);
		showInfo.addActionListener(this);
		closePanel.addActionListener(this);

		deleteCursor.addActionListener(this);
		clearCursor.addActionListener(this);

		NumberFormat format = NumberFormat.getIntegerInstance();
		format.setGroupingUsed(false);

		NumberFormatter numberFormatter = new NumberFormatter(format);
		numberFormatter.setValueClass(Long.class); 

		this.add(buttonPanel);
		resultPanel = new ResultPanel(acq);
		this.add(resultPanel);

		this.add(graphPanel);

	}


	public ResultPanel getResultPanel() {
		return resultPanel;
	}

	/**
	 * Gives the chartPanel which contains the chart entitled 'chartText'
	 * 
	 * @param chartText : title of the chart
	 * @return the according chartPanel
	 */
	public ChartPanel getChartPanel(String chartText){

		System.out.println("Retrieving chart "+chartText);

		ChartPanel chartPanel=null;

		//	ChartPanel
		for (int i = 0 ; i<graphPanel.getComponentCount();i++ ) {
			ChartPanel currentCP =(ChartPanel) graphPanel.getComponent(i);

			if ( currentCP.getChart().getTitle().getText().equals(chartText) )
				chartPanel = currentCP;
		}

		return chartPanel;
	}

	private ChartPanel getChartPanel(){
		return (ChartPanel) graphPanel.getComponent(0);
	}

	public void actionPerformed(ActionEvent e) {
		if ("DeleteCursor".equals(e.getActionCommand())) {
			cursorMode=DELETE_CURSOR;
		} else if ("addDebut".equals(e.getActionCommand())) {
			cursorMode=ADD_CURSOR;
		//	currentSeriesDescription=(String) GlobalConfiguration.getInstance().getParam(GlobalConfiguration.DESCRIPTION_EMG);
			curCursor="Start";
		} else if ("addFin".equals(e.getActionCommand())) {
			cursorMode=ADD_CURSOR;
			curCursor="End";
			//currentSeriesDescription=(String) GlobalConfiguration.getInstance().getParam(GlobalConfiguration.DESCRIPTION_EMG);
		} else if ("addMax".equals(e.getActionCommand())) {
			cursorMode=ADD_CURSOR;
			curCursor="Max";
		//	currentSeriesDescription=(String) GlobalConfiguration.getInstance().getParam(GlobalConfiguration.DESCRIPTION_EMG);
		} else if ("addMin".equals(e.getActionCommand())) {
			cursorMode=ADD_CURSOR;
			curCursor="Min";
		//	currentSeriesDescription=(String) GlobalConfiguration.getInstance().getParam(GlobalConfiguration.DESCRIPTION_EMG);
		} else if ("ClearCursor".equals(e.getActionCommand())) {
			cursorMode= DEFAULT_MODE;
			MainFrame.getInstance().getCursorCollection(currentAcq).clearCursor();
		} else if ("ComputeCursor".equals(e.getActionCommand())) {
			triggerCursor();
		} else if ("DebugInfo".equals(e.getActionCommand())) {
			JOptionPane.showMessageDialog(MainFrame.getInstance(), this.toString(),"Debug info",JOptionPane.WARNING_MESSAGE);
		} else if ("ClosePanel".equals(e.getActionCommand())){
			MainFrame.getInstance().removePanel(this);
		} else if ("CursorToZero".equals(e.getActionCommand())){

			MainFrame.getInstance().getCursorCollection(currentAcq).clearCursor();

			ChartPanel chartPanel = getChartPanel();
			JFreeChart chart = chartPanel.getChart();

			CursorProcessor.everyCursorToZero(currentAcq,chart,this);

		}
	}

	public void chartMouseClicked(ChartMouseEvent e) {

		double x = e.getTrigger().getX();

		JFreeChart chart =e.getChart();

		if (cursorMode==ADD_CURSOR) {
			Color curveColor =Color.BLUE;

			CursorGraph cursor = new CursorGraph();
			cursor.fillWithPixelValue(curCursor,curveColor,x,chart.getTitle().getText(),this,chart);
			MainFrame.getInstance().getCursorCollection(currentAcq).add(cursor);


		} else if (cursorMode==DELETE_CURSOR) {
			CursorGraph closestCursor = MainFrame.getInstance().getCursorCollection(currentAcq).findCursor(GraphUtils.pixel2graph(x,chart),chart.getTitle().getText(),chart);
			if (closestCursor!=null){
				closestCursor.eraseCursor();
				MainFrame.getInstance().getCursorCollection(currentAcq).removeCursor(closestCursor);
			}

			cursorMode= DEFAULT_MODE;
		} else if (cursorMode== DEFAULT_MODE){
			System.out.println("Coord Graph "+GraphUtils.pixel2time(x,e.getChart()));

		}
	}


	public void chartMouseMoved(ChartMouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	
	public void triggerCursor() {
		//Erasing existing one 
		MainFrame.getInstance().getCursorCollection(currentAcq).clearCursor();

		ChartPanel chartPanel = getChartPanel();
		JFreeChart chart = chartPanel.getChart();

		CursorProcessor.computeCursors(currentAcq,chart,this);
	}

	public Acquisition getCurrentAcquisition(){
		return currentAcq;
	}

	@Override
	public String toString() {
		return "DataPanel{" +
				" acquisition =" + currentAcq +
				", resultPanel =" + resultPanel +
				'}';
	}
}
