package eu.askadev.curseurmep.gui.graph;


import java.awt.Color;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;

import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import eu.askadev.curseurmep.conf.GlobalConfiguration;
import eu.askadev.curseurmep.model.Acquisition;

import static eu.askadev.curseurmep.conf.GlobalConfiguration.DESCRIPTION_EMG;
import static eu.askadev.curseurmep.utils.ComputationUtils.computeTime;


/**
 * @author pvincen
 * construct the graph displayed, behaves as a chart generator
 * Listens to the mouse event on both charts
 */
public class DataChartManager{

	/**********************************************************
	 *****************   Data to be displayed ***************** 
	 **********************************************************/
	private XYSeries seriesEMG;

	private Acquisition currentAcq;
	private HashMap<XYSeries,GraphCurve> curveConfig= new HashMap<XYSeries,GraphCurve>();

	/***********************************************
	 *****************   Singleton ***************** 
	 ***********************************************/

	//Accessors
//TODO virer tt ce bordel

	public DataChartManager() {
		super();
		seriesEMG = new XYSeries("EMG");

	}


	public ChartPanel getChart(int rank,String title){

		final JFreeChart chart = createChart(createDataset(rank),rank,title);


		final ChartPanel chartPanel = new ChartPanel(chart);

		chartPanel.setPreferredSize(new java.awt.Dimension(1200, 700));
		//	chartPanel.setMouseWheelEnabled(true);
		chartPanel.setMouseZoomable(true);

		Plot plot = chart.getPlot();



		//setContentPane(chartPanel);

		return chartPanel;
	}



	/**
	 * Creates a chart.
	 *
	 * @param dataset  the data for the chart.
	 *
	 * @return a chart.
	 */
	private JFreeChart createChart(XYSeriesCollection dataset, int rang,String title) {

		// create the chart...
		final JFreeChart chart = ChartFactory.createXYLineChart(
				title,      // chart title
				"X",                      // x axis label
				"Y",                      // y axis label
				dataset,                  // data
				PlotOrientation.VERTICAL,
				true,                     // include legend
				true,                     // tooltips
				false                     // urls
		);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		chart.setBackgroundPaint(Color.white);

		// get a reference to the plot for further customisation...
		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.lightGray);
		//    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.addChangeListener(chart);
		NumberAxis domain = (NumberAxis) plot.getDomainAxis();
		domain.setRange(0.00, 100);


		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true,false);


		for (int i = 0; i< dataset.getSeriesCount();i++) {
			renderer.setSeriesPaint(i,curveConfig.get(dataset.getSeries(i)).getColor());
		}

		plot.setRenderer(renderer);

		// change the auto tick unit selection to integer units only...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		//plot.getDomainAxis().setRange(0, dataset.getItemCount(0));
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());



		// OPTIONAL CUSTOMISATION COMPLETED.

		return chart;

	}

	/**
	 * Creates the dataset with the data wrapped in Acquisition
	 *
	 * @return the dataset
	 */
	private XYSeriesCollection createDataset(int rang) {


		XYSeriesCollection dataset = new XYSeriesCollection();

		//Later, will read from a file
		GraphCurve blue = new GraphCurve("blue",Color.BLUE,3);
		dataset.addSeries(seriesEMG);
		curveConfig.put(seriesEMG, blue);




		return dataset;

	}


	public void initDataset(Acquisition acq) {

		if ( acq==null || acq.getEMG() ==null)
			return;

		seriesEMG = new XYSeries("EMG");
		seriesEMG.setDescription((String)GlobalConfiguration.getInstance().getParam(DESCRIPTION_EMG));

		currentAcq = acq;

		for (int i = 0; i < currentAcq.getEMG().size();i++) {
			seriesEMG.add(computeTime(100,i),currentAcq.getEMG().get(i));
		}
	}

	public HashMap<XYSeries, GraphCurve> getCurveConfig(){

		return curveConfig;
	}


}
