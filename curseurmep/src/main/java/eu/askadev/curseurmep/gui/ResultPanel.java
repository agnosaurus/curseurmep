package eu.askadev.curseurmep.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import org.jfree.chart.plot.ValueMarker;

import eu.askadev.curseurmep.export.ExportData;
import eu.askadev.curseurmep.gui.graph.CursorCollection;
import eu.askadev.curseurmep.gui.graph.CursorGraph;
import eu.askadev.curseurmep.gui.graph.GraphUtils;
import eu.askadev.curseurmep.model.Acquisition;

public class ResultPanel  extends JPanel implements ActionListener {


	private JPanel fieldPanel;
	private Acquisition acquisition;

	/**
	 *  - duree : fin - debut
	 - valeur Y min / max
	 - amplitude peak 2 peak
	 - aire : difference avec 0 , valeur absolue
	 */

	//Resultats a compute:
	double duration=0d;
	double min=0d;
	double max =0d;
	double amplitude=0d;
	double area=0d;
	double baseline=-50d;

	//Bouton
	JButton buttonExport;
	JButton buttonResult;


	double dataTimeInc = 100d/4800d;

	public ResultPanel(Acquisition acquisition){

		super();

		this.acquisition = acquisition;
		this.setLayout(new FlowLayout());


		//Create panel for button
		fieldPanel = new JPanel(new FlowLayout());
		fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.Y_AXIS));
		fieldPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));


		JLabel labelTotalDuration=new JLabel("Duree totale:");

		JLabel labelMax=new JLabel("Max:");
		JLabel labelMin=new JLabel("Min:");
		JLabel labelAmplitude=new JLabel("Amplitude:");
		JLabel labelDuration=new JLabel("Duree:");
		JLabel labelArea=new JLabel("Aire:");
		JLabel labelBaseline=new JLabel("Baseline:");




		fieldPanel.add(labelTotalDuration);
		fieldPanel.add(labelMax);
		fieldPanel.add(labelMin);
		fieldPanel.add(labelAmplitude);
		fieldPanel.add(labelDuration);
		fieldPanel.add(labelArea);
		fieldPanel.add(labelBaseline);



		buttonResult = new JButton("Compute Result");
		buttonResult.setActionCommand("result");
		buttonResult.addActionListener(this);

		fieldPanel.add(buttonResult);

		buttonExport = new JButton("Export Result");
		buttonExport.setActionCommand("export");
		buttonExport.addActionListener(this);

		fieldPanel.add(buttonExport);


		this.add(fieldPanel);
	}

	public int getMaxTime(){
		return getMainframe().getControlPanel().getTotalTime();
	}

	public void computeResult(){
		computeResult(getMaxTime());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("result".equals(e.getActionCommand())) {
			computeResult();
		} else if ("export".equals(e.getActionCommand())) {
			exportResult();
		}

	}

	private MainFrame getMainframe(){
		return MainFrame.getInstance();
	}

	public void exportAutomaticResult(){
		try {
			exportResult(new File(getMainframe().getLatestFileDir().getPath()+File.separator+acquisition.getFilename()+".auto.csv"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportResult(){

		//File Chooser
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(getMainframe().getLatestFileDir());

		//TODO display errors
		chooser.setSelectedFile(new File(acquisition.getFilename()+".csv"));
		int retrieval = chooser.showSaveDialog(null);
		if (retrieval == JFileChooser.APPROVE_OPTION) {

			try {
				exportResult(chooser.getSelectedFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void exportResult(File outputFile)throws IOException{

		try(FileWriter fw = new FileWriter(outputFile+".csv")) {
			fw.write("Min mV,"+roundDouble(min)+"\n");
			fw.write("Max mV,"+roundDouble(max)+"\n");
			fw.write("Amplitude mV,"+roundDouble(amplitude)+"\n");
			fw.write("Duration ms,"+roundDouble(duration)+"\n");
			fw.write("Area mV*ms,"+roundDouble(area)+"\n");
			fw.write("Baseline mV,"+roundDouble(baseline));
		}
	}

	public void computeResult(double maxTime){
		getMainframe().setDataPanelColor(acquisition.getFilename(),Color.BLACK);

		double factor = maxTime/100d ;

		System.out.println("Computing result with a factor of "+factor);
		System.out.println("Computing result for acquisition of " + acquisition);


		try{

			CursorCollection cursorCollection = MainFrame.getInstance().getCursorCollection(acquisition);
			System.out.println("Getting cursor collection " + acquisition);


			CursorGraph maxCursor = cursorCollection.stream().filter(cursor -> cursor.getLabel().equals("Max") ).findFirst().get();
			CursorGraph minCursor = cursorCollection.stream().filter(cursor -> cursor.getLabel().equals("Min") ).findFirst().get();


			double max = acquisition.getEMG().get(GraphUtils.graph2data(((ValueMarker) maxCursor.getMarker()).getValue()));
			double min = acquisition.getEMG().get(GraphUtils.graph2data(((ValueMarker) minCursor.getMarker()).getValue()));

			System.out.println("Min/Max "+min+" // "+max);

			this.max=max;
			this.min=min;
			this.amplitude = Math.abs(max) +  Math.abs(min) ;

			//Duration
			CursorGraph startCursor = cursorCollection.stream().filter(cursor -> cursor.getLabel().equals("Start") ).findFirst().get();
			CursorGraph endCursor = cursorCollection.stream().filter(cursor -> cursor.getLabel().equals("End") ).findFirst().get();


			this.duration = factor * ( ((ValueMarker) endCursor.getMarker()).getValue() -  ((ValueMarker) startCursor.getMarker()).getValue() );

			//Area
			int dataStart = GraphUtils.graph2data(((ValueMarker) startCursor.getMarker()).getValue());
			int dataEnd = GraphUtils.graph2data(((ValueMarker) endCursor.getMarker()).getValue());

			double area=0d;

			for ( int i = dataStart ; i< dataEnd ; i++)
				area+=  factor * Math.abs(dataTimeInc * acquisition.getEMG().get(i));


			this.area=area;
			this.baseline = cursorCollection.getBaseline();

			refreshLabel();


		} catch ( Exception e){
			getMainframe().setDataPanelColor(acquisition.getFilename(),Color.RED);
			JOptionPane.showMessageDialog(getMainframe(), "Please set the cursors","Error",JOptionPane.ERROR_MESSAGE);
		}

	}


	public String roundDouble(double input){
		DecimalFormat df = new DecimalFormat("#####.##");
		df.setRoundingMode(RoundingMode.HALF_UP);

		return df.format(input);
	}

	public void refreshLabel(){
		(( JLabel)fieldPanel.getComponent(0)).setText("Duree totale: "+getMaxTime()+" ms");
		(( JLabel)fieldPanel.getComponent(1)).setText("Max: "+roundDouble(max)+" mV");
		(( JLabel)fieldPanel.getComponent(2)).setText("Min: "+roundDouble(min)+" mV");
		(( JLabel)fieldPanel.getComponent(3)).setText("Amplitude: "+roundDouble(amplitude)+" mV");
		(( JLabel)fieldPanel.getComponent(4)).setText("Duree: "+roundDouble(duration)+" ms");
		(( JLabel)fieldPanel.getComponent(5)).setText("Aire:"+roundDouble(area)+" ms*mV");
		(( JLabel)fieldPanel.getComponent(6)).setText("Baseline:"+roundDouble(baseline)+" mV");
	}


	public ExportData getExportData() {
		return new ExportData(amplitude, duration, area);
	}

	@Override
	public String toString() {
		return "ResultPanel{" +
				"acquisition=" + acquisition +
				", amplitude=" + amplitude +
				'}';
	}
}
