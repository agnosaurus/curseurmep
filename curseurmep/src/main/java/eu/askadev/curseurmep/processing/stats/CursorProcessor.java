package eu.askadev.curseurmep.processing.stats;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import eu.askadev.curseurmep.utils.ProcessorUtils;

import org.jfree.chart.JFreeChart;

import eu.askadev.curseurmep.gui.DataPanel;
import eu.askadev.curseurmep.gui.MainFrame;
import eu.askadev.curseurmep.gui.graph.CursorGraph;
import eu.askadev.curseurmep.model.Acquisition;

import static eu.askadev.curseurmep.processing.TimeDirection.*;
import static eu.askadev.curseurmep.utils.ProcessorUtils.computeWindowMean;
import static eu.askadev.curseurmep.utils.ProcessorUtils.findTimeAfterSpike;

public class CursorProcessor {


	//TODO regler proprement l echantillonage
	/**	public static final Map<Integer,Integer> PULSE_LIMITS = new HashMap<Integer,Integer>()
	 {{
	 put(100, 796);
	 put(200, 1200);
	 }};
	 */
	public static final double LVL_PERCENT = 0.20d;
	public static final double END_LVL_PERCENT = 0.15d;

	public static final double START_WINDOW_MEAN_TRIGGER =0.5d;
	public static final double END_WINDOW_MEAN_TRIGGER =0.05d;


	public static final int START_WINDOW_SIZE = 15;
	public static final int END_WINDOW_SIZE = 5;

	public static void everyCursorToZero(Acquisition acq,JFreeChart chart, DataPanel dataPanel ){
		addCursor(0,"Max",Color.RED,acq,chart,dataPanel);
		addCursor(0,"Min",Color.RED,acq,chart,dataPanel);

		addCursor(0,"End",Color.BLACK,acq,chart,dataPanel);
		addCursor(0,"Start",Color.BLACK,acq,chart,dataPanel);

	}

	public static void computeCursors(Acquisition acq,JFreeChart chart, DataPanel dataPanel ){

		int totalTime = MainFrame.getInstance().getControlPanel().getTotalTime();

		// Min-Max
		int indexMin=0;
		int indexMax =0;
		double min = 200000;
		double max = -200000;

		//TODO mieux determiner le temps de debut... on vire les pentes super elevées


		//Calcul derivee
		ArrayList<Double> derivate = new ArrayList<>();
		for ( int i =1; i< acq.getEMG().size() ; i++){
			double curVal = acq.getEMG().get(i);
			double previousVal = acq.getEMG().get(i-1);

			derivate.add(curVal - previousVal);
		}

		int timeAfterPeek = findTimeAfterSpike(derivate);
		System.out.println("timeAfterPeek "+timeAfterPeek);

		// Min Max
		for ( int i = timeAfterPeek ; i< acq.getEMG().size() ; i++){

			double curVal = acq.getEMG().get(i);
			if (curVal < min){
				min = curVal;
				indexMin=i;
			} else if (curVal > max){
				max = curVal;
				indexMax=i;
			}

		}

		// TODO : on calcule un genre de derivee, pour voir le vrai point d inflexion


		System.out.println("Cursor processor Min // Max"+ min+" // "+max);

		//TODO : change this logic, voir avec Steph
		// USED ONLY WHEN 200 ms = total
		// so 4800 points => 200 ms
		// 480 => 20 ms
		// 120 => 5
		double baseline =0;
		for ( int i =120 ; i<480+120+1; i++){ // between 5 - 25 ms
			baseline += acq.getEMG().get(i);
		}

		baseline = baseline/480;

		//TODO : a remplacer par un appel avec Acquisition comme cle
		MainFrame.getInstance().getCursorCollection(acq).setBaseline(baseline);


		addCursor(indexMax,"Max",Color.RED,acq,chart,dataPanel);
		addCursor(indexMin,"Min",Color.RED,acq,chart,dataPanel);


		//Check if which is first
		//	double startAmplitude1=0d;
		double endAmplitude=0d;

		int startIndex=0;
		int endIndex_Method1=0;
		int endIndex_Method2=0;


		// We start when moving more than 20% of initial
		//startAmplitude=computeWindowMean(acq.getEMG(),timeAfterPeek+30,30, PAST)*1.2d;

		if ( indexMax < indexMin){
			//	startAmplitude = Math.abs(START_LVL_PERCENT *  max);
			endAmplitude = Math.abs(END_LVL_PERCENT * min);
		} else {
			//	startAmplitude = Math.abs(START_LVL_PERCENT * min);
			endAmplitude = Math.abs(END_LVL_PERCENT * max);
		}

		//System.out.println("startAmplitude " + startAmplitude);
		System.out.println("endAmplitude " + endAmplitude );



		// ON va avancer jusqu à avoir un decrochage sur les derivées qui se confirme sur suffisament longtemps
		for ( int i = timeAfterPeek ; i< derivate.size() ; i++){
			double windowMean = computeWindowMean(derivate,i, START_WINDOW_SIZE,PAST);
			if ( Math.abs(windowMean) > START_WINDOW_MEAN_TRIGGER){
				startIndex=i - START_WINDOW_SIZE;
				break;
			}

		}

		double endValue = computeWindowMean(acq.getEMG(),acq.getEMG().size()-1, 500,PAST);
		double startValue = computeWindowMean(acq.getEMG(),startIndex, 5,PRESENT);


		int endIndexStart = (indexMax > indexMin ? indexMax : indexMin) +END_WINDOW_SIZE+10;

		// ON va avancer jusqu à ne plus avoirde decrochage sur les derivées qui se confirme sur suffisament longtemps
		for ( int i = endIndexStart ; i < derivate.size() - END_WINDOW_SIZE ; i++){
			double windowMeanFuture = computeWindowMean(derivate,i, END_WINDOW_SIZE,FUTURE);
			double windowMeanPast = computeWindowMean(derivate,i, END_WINDOW_SIZE,PAST);

			if ( Math.abs(windowMeanFuture) < END_WINDOW_MEAN_TRIGGER
					&& Math.abs(windowMeanPast) < END_WINDOW_MEAN_TRIGGER
					&& Math.abs(acq.getEMG().get(i) - startValue) / startValue < LVL_PERCENT){
				endIndex_Method1=i;
				break;
			}

		}


		//compute end
		for ( int i =1 ; i<= acq.getEMG().size() ; i++){

			double curVal = acq.getEMG().get(acq.getEMG().size() -i);

			if ( Math.abs(curVal) > endAmplitude ){
				endIndex_Method2 = acq.getEMG().size() - i;
				break;

			}
		}

	//	System.out.println("METHOD 1 "+endIndex_Method1);
	//	System.out.println("METHOD 2 "+endIndex_Method2);





		if ( Math.abs(endIndex_Method1 - endIndex_Method2) > 100){
			addCursor(endIndex_Method1,"End",Color.BLACK,acq,chart,dataPanel);
			addCursor(endIndex_Method2,"End",Color.BLACK,acq,chart,dataPanel);
		} else {
			int endIndex=Math.max(endIndex_Method2,endIndex_Method1);
			addCursor(endIndex,"End",Color.BLACK,acq,chart,dataPanel);

		}

		addCursor(startIndex,"Start",Color.BLACK,acq,chart,dataPanel);

	}


	private static void addCursor(int endIndex,String label,Color color,Acquisition acq,JFreeChart chart, DataPanel dataPanel ){

		CursorGraph cursor = new CursorGraph();
		cursor.fillCursorWithDataValue(label,color,endIndex,chart.getTitle().getText(),dataPanel,chart);
		MainFrame.getInstance().getCursorCollection(acq).add(cursor);
	}



}
