package eu.askadev.curseurmep.processing;

public enum TimeDirection {
    PAST,
    FUTURE,
    PRESENT
}
