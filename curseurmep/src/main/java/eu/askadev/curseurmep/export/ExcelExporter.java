package eu.askadev.curseurmep.export;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import eu.askadev.curseurmep.gui.MainFrame;
import eu.askadev.curseurmep.utils.BlocName;
import eu.askadev.curseurmep.utils.FileNameUtils;





public class ExcelExporter {

	public static final int DATA_WIDTH =6;

	
	//COLUMN
	public static final int COL_AMPLITUDE =0;
	public static final int COL_DURATION = 7;
	public static final int COL_AREA = 14;
	
	
	
	private static ExcelExporter instance = null;


	private ExcelExporter() {
	}

	public static ExcelExporter getInstance() {

		if (instance==null)
			instance = new ExcelExporter();

		return instance;
	}
	
	
	public static void copyFile(File input, File output) {
		try {
			FileUtils.copyFile(input, output);
		} catch (IOException exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
		}
	}


    /**
     * creates an {@link HSSFWorkbook} with the specified OS filename.
     */
    public HSSFWorkbook readFile(String filename)  {
        try (FileInputStream fis = new FileInputStream(filename)) {
            return new HSSFWorkbook(fis);      
        } catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(MainFrame.getInstance(), e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(MainFrame.getInstance(), e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		return null;
    }
    
	
	public Workbook openFile(File file) {
		
		
		
		//Make a SAVE of file before
		File copied = new File(file.getPath()+".SAVE");

		copyFile(file,copied);
		
		Workbook wb=null;
		try {
			wb = WorkbookFactory.create(copied);
		} catch (EncryptedDocumentException | IOException | InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return wb;

	}
	
	public int computeRow(BlocName blocName, int localIndex) {
		return blocName.getStartRow()+localIndex;
	}
	


	public void writeData(Workbook wb,File file,ExportDestination exportDestination, ExportData exportData) {

		
		List<String> sheetNames = FileNameUtils.getSheetNamesFromDirName(exportDestination.getDirName());

		// on va chercher la sheet qui va bien
		Sheet sheet = getSheet(sheetNames,wb);

		// On va chercher le row, puis la colonne
		Row row = sheet.getRow(computeRow(exportDestination.getBlocName(), exportDestination.getPositionInTheSerie()));
		
		// On va chercher les colonnes sur les 3 datas
		Cell amplitudeCell = row.getCell(COL_AMPLITUDE+exportDestination.getColumnName().getColumnNb());
		Cell durationCell = row.getCell(COL_DURATION+exportDestination.getColumnName().getColumnNb());
		Cell areaCell = row.getCell(COL_AREA+exportDestination.getColumnName().getColumnNb());
		
		
		amplitudeCell.setCellValue(exportData.getAmplitude());
		durationCell.setCellValue(exportData.getDuration());
		areaCell.setCellValue(exportData.getArea());
		
	}

	public Sheet getSheet(List<String> sheetNames,Workbook wb){	
		return sheetNames.stream().map(title -> wb.getSheet(title)).findFirst().orElse(null);
	}


	//TODO : faire un save + clean
	public void saveWorkbook(Workbook wb, File file) throws FileNotFoundException, IOException {

System.out.println("Saving "+file.getPath());
	//	File updatedFile = new File(file.getPath().replaceAll(".xls", "UPDATED.xls"));
		
		try (OutputStream fileOut = new FileOutputStream(file)) {
			wb.write(fileOut);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(MainFrame.getInstance(), e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
		}
		
		wb.close();
	}





	/**

	try (InputStream inp = new FileInputStream("workbook.xls")) {
		//InputStream inp = new FileInputStream("workbook.xlsx");
		    Workbook wb = WorkbookFactory.create(inp);
		    Sheet sheet = wb.getSheetAt(0);
		    Row row = sheet.getRow(2);
		    Cell cell = row.getCell(3);
		    if (cell == null)
		        cell = row.createCell(3);
		    cell.setCellType(CellType.STRING);
		    cell.setCellValue("a test");
		    // Write the output to a file
		    try (OutputStream fileOut = new FileOutputStream("workbook.xls")) {
		        wb.write(fileOut);
		    }
		}

	 */

}
