package eu.askadev.curseurmep.export;

public class ExportData {

	double amplitude;
	double duration;
	double area;
	
	public ExportData(double amplitude, double duration, double area) {
		super();
		this.amplitude = amplitude;
		this.duration = duration;
		this.area = area;
	}

	public double getAmplitude() {
		return amplitude;
	}

	public void setAmplitude(double amplitude) {
		this.amplitude = amplitude;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}
	
	
	
}
