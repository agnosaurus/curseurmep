package eu.askadev.curseurmep.export;

import java.io.File;
import java.util.List;

import eu.askadev.curseurmep.gui.MainFrame;
import eu.askadev.curseurmep.utils.BlocName;
import eu.askadev.curseurmep.utils.ColumnName;
import eu.askadev.curseurmep.utils.FileNameUtils;

public class ExportDestination {


	List<String> sheetNames;
	BlocName blocName; //SICI_7 etc
	ColumnName columnName;
	int positionInTheSerie;
	String dirName;


	public ExportDestination() {
		super();
	}

	public ExportDestination(File file) {

		String dirName = FileNameUtils.getDirNameFromPath(file.getAbsolutePath());
		
		//TODO : add a check on the cursors + results

		setSheetName(FileNameUtils.getSheetNamesFromDirName(dirName));
		setBlocName(BlocName.getBlocName(FileNameUtils.getAcquisitionType(FileNameUtils.getFilenameFromFile(file))));
		setColumnName(FileNameUtils.getColumnNameFromDirName(dirName));


		List<File> files = MainFrame.getInstance().getControlPanel().getOpenedFiles();


		int localIndex = FileNameUtils.computeLocalIndex(file, files);
		setPositionInTheSerie(localIndex);
		
		this.dirName = dirName;


	}


	public List<String> getSheetNames() {
		return sheetNames;
	}

	public void setSheetName(List<String> sheetNames) {
		this.sheetNames = sheetNames;
	}

	public ColumnName getColumnName() {
		return columnName;
	}


	public void setColumnName(ColumnName columnName) {
		this.columnName = columnName;
	}

	public BlocName getBlocName() {
		return blocName;
	}
	public void setBlocName(BlocName blocName) {
		this.blocName = blocName;
	}
	public int getPositionInTheSerie() {
		return positionInTheSerie;
	}
	public void setPositionInTheSerie(int positionInTheSerie) {
		this.positionInTheSerie = positionInTheSerie;
	}

	public String getDirName() {
		return dirName;
	}


}
