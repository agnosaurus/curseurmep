package eu.askadev.curseurmep.conf;

import java.util.HashMap;

//Knows some general config parameters
public class GlobalConfiguration extends HashMap{
	

	private static GlobalConfiguration instance;
	
	public static GlobalConfiguration getInstance(){
		if (instance == null)
			instance = new GlobalConfiguration();
		
		return instance;
	}
	
	public static final String DESCRIPTION_EMG="DESCRIPTION_EMG";
	
	public GlobalConfiguration() {
		super();
		
		this.put(DESCRIPTION_EMG, "blue");
	
		
	}
	
	public Object getParam(String key){
		
		return this.get(key);
	}
	
	
	
	
	
}
