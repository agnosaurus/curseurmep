package eu.askadev.curseurmep.utils;


import org.junit.jupiter.api.Test;

import static eu.askadev.curseurmep.utils.ComputationUtils.computeTime;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComputationUtilsTest {

    @Test
    public void testcomputeTime(){
        assertEquals(100d, computeTime(100,4800));
        assertEquals(0d, computeTime(100,0));

    }
}
