package eu.askadev.curseurmep.utils;


import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileNameUtilsTest {




	private static Stream<Arguments> filepaths() {
		return Stream.of(
				Arguments.of("D:\\work\\sub-P058-1967\\sub-P058-1967_t01_baseline\\ICI-ICF\\-\\Right\\Baseline--Muscle__001" , "sub-P058-1967_t01_baseline", List.of("P058_","P058_1967"))
				);
	} 

	private static Stream<Arguments> filenames() {
		return Stream.of(
				Arguments.of("Baseline--Muscle__026" , "Baseline",26),
				Arguments.of("SICI_3ms--Muscle__005" , "SICI_3",5),
				Arguments.of("ICF_15ms--Muscle__049" , "ICF_15",49),
				Arguments.of("ICF_7ms--Muscle__155" , "ICF_7",155)
				);
	} 

	private static Stream<Arguments> dirname_columnNames() {
		return Stream.of(
				Arguments.of("sub-P040_t01_baseline" , ColumnName.Baseline_1),
				Arguments.of("sub-P040_t02_baseline2" , ColumnName.Baseline_2),
				Arguments.of("sub-P040_t03_postTMS" , ColumnName.Post_TMS),
				Arguments.of("sub-P040_t04_FU1" , ColumnName.FU1),
				Arguments.of("sub-P040_t04_FU2" , ColumnName.FU2)
				);
	} 


	private static Stream<Arguments> integerList() {
		return Stream.of(
				Arguments.of(List.of(1,2,3,4,5,6,7,8,9,10,11,12,13) ,List.of(1,5,9,13)),
				Arguments.of(List.of(1,2,4,5,6,11,12,13) ,List.of(1,5,11)),
				Arguments.of(List.of(1,4,5,6,7,8,10,11,12,13) ,List.of(1,5,10)),
				Arguments.of(List.of(1,2,3,4,6,7,8,9,10,11,12,13) ,List.of(1,6,10))
				);
	} 



	private static Stream<Arguments> localIndex() {
		return Stream.of(
				Arguments.of(1,0),
				Arguments.of(2,1),
				Arguments.of(3,2),
				Arguments.of(4,3),
				Arguments.of(7,1),
				Arguments.of(11,1)
	
				);
	} 
	
	
	



	@ParameterizedTest
	@MethodSource("filepaths")
	public void testgetDirNameFromPath(String input,String expected, List<String> expectedSheets) {
		String dirName = FileNameUtils.getDirNameFromPath(input);
		assertEquals(expected,dirName);
	}



	@ParameterizedTest
	@MethodSource("filepaths")
	public void testgetsheetNamesFromPath(String inputDir,String expectedDirName, List<String> expectedSheets) {		
		List<String> sheetNames = FileNameUtils.getSheetNamesFromDirName(expectedDirName);
		sheetNames.stream().forEach(sheetName -> assertTrue(expectedSheets.contains(sheetName), sheetName+" not in list"));
		expectedSheets.stream().forEach(sheetName -> assertTrue(sheetNames.contains(sheetName), sheetName+" not in list"));

	}

	@ParameterizedTest
	@MethodSource("filenames")
	public void testgetAcquisitionType(String input, String expected, int notUsed) {
		String acqType = FileNameUtils.getAcquisitionType(input);
		assertEquals(expected, acqType);	
	}


	@ParameterizedTest
	@MethodSource("dirname_columnNames")
	public void testgetColumnNameFromDirName(String filename, ColumnName expected) {
		ColumnName result = FileNameUtils.getColumnNameFromDirName(filename);
		assertEquals(expected, result);
	}


	@ParameterizedTest
	@MethodSource("filenames")
	public void testgetIndexFromFilename(String filename, String notUsed, int expected) {
		int result = FileNameUtils.getIndexFromFilename(filename);
		assertEquals(expected, result);
	}

}
